# Install docker, docker-compose

# Run docker compose

```docker-compose up```

# Install knex globally
```npm i knex -g```

# Run migration

```npm run migrate```

# Run seed (default password for users - "hashvalue")

```npm run seed```



# Further improvements

* add swagger
* better error handling
* setup different environments
* add tests
* fully dockerize