import type { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('users', table => {
        table.increments('id').primary();
        table.enum('role', ['USER', 'MODERATOR', 'ADMIN'], {
            useNative: true,
            enumName: 'role_enum'
        });
        table.enum('status', ['ACTIVE', 'BANNED'], {
            useNative: true,
            enumName: 'user_status_enum',
        });
        table.text('phone');
        table.text('full_name');
        table.text('email');
        table.text('password');

        table.unique('email', {
            storageEngineIndexType: 'hash'
        });
        table.unique('phone', {
            storageEngineIndexType: 'hash'
        });
    });
}


export async function down(knex: Knex): Promise<void> {
}

