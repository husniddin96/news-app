import type { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('sms_codes', table => {
        table.increments('id').primary();
        table.text('phone');
        table.text('code');
        table.boolean('used');
        table.unique(['phone', 'code'], {
            storageEngineIndexType: 'hash'
        });
    });
}



export async function down(knex: Knex): Promise<void> {
}

