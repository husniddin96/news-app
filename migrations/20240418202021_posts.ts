import type { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('posts', table => {
        table.increments('id').primary();
        table.text('title');
        table.text('cover');
        table.text('description');
        table.text('full_text');
        table.integer('author_id').unsigned(); // Define author_id column as integer
        table.foreign('author_id').references('id').inTable('users');
        table.enum('status', ['DRAFT', 'PUBLISHED'], {
            useNative: true,
            enumName: 'post_status'
        });
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('posts');
}

