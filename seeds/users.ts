import { Knex } from "knex";

enum UserRole {
    ADMIN = 'ADMIN',
    MODERATOR = 'MODERATOR',
    USER = 'USER',
}

enum UserStatus {
    ACTIVE = 'ACTIVE',
    BANNED = 'BANNED'
}

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    // await knex("users").del();

    // Inserts seed entries
    await knex("users").insert([
        {
            id: 1,
            role: UserRole.ADMIN,
            status: UserStatus.ACTIVE,
            phone: '+998971000000',
            full_name: 'John the Admin',
            email: 'jadmin@mail.com',
            password: '$2b$10$g/LRk7WAP6GP8y/vU5jKPOaFI4iiCtztvlWfYuhQrfM2FiqnbRheS',
        },
        {
            id: 2,
            role: UserRole.MODERATOR,
            status: UserStatus.ACTIVE,
            phone: '+998971000001',
            full_name: 'Tom the Moderator',
            email: 'tmod@mail.com',
            password: '$2b$10$g/LRk7WAP6GP8y/vU5jKPOaFI4iiCtztvlWfYuhQrfM2FiqnbRheS',
        },
        {
            id: 3,
            role: UserRole.USER,
            status: UserStatus.ACTIVE,
            phone: '+998971000002',
            full_name: 'Mike the User',
            email: 'muser@mail.com',
            password: '$2b$10$g/LRk7WAP6GP8y/vU5jKPOaFI4iiCtztvlWfYuhQrfM2FiqnbRheS',
        },
    ]);
};
