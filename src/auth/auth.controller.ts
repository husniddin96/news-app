import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { SMSCodeGuard } from 'src/guards/sms-code.guard';
import { LoginDTO } from './auth.interface';
import { AuthService } from './auth.service';
import { SMSService } from './sms.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService,
    private readonly smsService: SMSService) { }
  
  @UseGuards(SMSCodeGuard)
   @Post('login')
  create(@Body() credentials: LoginDTO) {
    return this.authService.login(credentials);
  }

  @Get('sms-code/:phone')
  getSMSCode(@Param('phone') phone: string) {
    return this.smsService.generateOTP(phone);
  }
}
