import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from 'src/users/users.module';
import { UsersService } from 'src/users/users.service';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { SMSService } from './sms.service';
import { SMSRepo } from './sms.repo';
import { DBModule } from 'src/database/db.module';

@Module({
  imports: [UsersModule, JwtModule, DBModule],
  controllers: [AuthController],
  providers: [AuthService, UsersService, SMSService, SMSRepo, JwtService],
})
export class AuthModule { }
