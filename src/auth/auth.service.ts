import { Injectable, UnauthorizedException, UseGuards } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import { LoginDTO } from './auth.interface';

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService,
    ) { }

    async login(credentials: LoginDTO): Promise<{ accessToken: string }> {
        const { email, password } = credentials;
        const user = await this.userService.findByEmail(email);

        const saltOrRounds = 10;
        const hash = await bcrypt.hash(password, saltOrRounds);
        try {
            await bcrypt.compare(hash, user.password);
            const payload = {
                sub: user.id,
                username: user.full_name,
                role: user.role,
                status: user.status
            };
            return {
                accessToken: await this.jwtService.signAsync(payload, { secret: process.env.JWT_SECRET }),
            };

        } catch (error) {
            throw new UnauthorizedException(error);
        }
    }

}
