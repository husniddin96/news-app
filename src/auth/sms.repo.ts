import { Injectable } from '@nestjs/common';
import { KnexService } from 'src/database/db.service';
import { SmsDTO } from './sms.service';

@Injectable()
export class SMSRepo {
    private readonly tableName = 'sms_codes';
    constructor(
        private readonly knexService: KnexService,
    ) { }

    async findByPhone(phone: string) {
        const knex = this.knexService.getInstance();
        const result = await knex(this.tableName)
            .select('*')
            .where('phone', phone)
            .andWhere('used', true);

        return result;
    }

    async create(smsCode: SmsDTO) {
        const knex = this.knexService.getInstance();
        const result = await knex(this.tableName)
            .insert(smsCode);

        return result;
    }

    async invalidateCode(id, data) {
        const knex = await this.knexService.getInstance();

        const result = await knex(this.tableName)
            .update(data)
            .where('id', id);

        return result;
    }

    async getNotUsedCode(phone: string) {
        const knex = this.knexService.getInstance();
        const result = await knex(this.tableName)
            .select('*')
            .where('phone', phone)
            .andWhere('used', false)
            .first();

        return result;
    }

}
