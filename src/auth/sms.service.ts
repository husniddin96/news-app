// TODO: to simulate sms sending logic
import { Injectable } from '@nestjs/common';
import { setTimeout } from 'timers/promises';
import { SMSRepo } from './sms.repo';

export interface SmsDTO {
    id?: number;
    code?: string;
    phone?: string;
    used?: boolean
}

@Injectable()
export class SMSService {
    constructor(private readonly repo: SMSRepo) { }

    async generateOTP(phone: string) {
        const codes = await this.repo.findByPhone(phone);
        const codesMap = codes.reduce((prev, curr) => {
            prev[curr.code] = true;
            return prev;
        }, {})

        const randomNumber = await this.recurse(codesMap);
        const smsCode = {
            phone,
            code: randomNumber,
            used: false,
        };

        await this.repo.create(smsCode);

        return randomNumber;
    }

    async checkOTP(phone, code): Promise<boolean> {
        const { code: notUsedCode, id } = await this.repo.getNotUsedCode(phone);

        if (code == notUsedCode) {
            const smsUpd = {
                used: true
            };

            await this.repo.invalidateCode(id, smsUpd);
            return true;
        }
        return false;
    }

    async recurse(codes: Record<string, boolean>): Promise<string> {
        const randomNumber = String(Math.random() * 1e7).slice(0, 6);
        if (codes[randomNumber]) {
            await setTimeout(1);
            return this.recurse(codes);
        }

        return randomNumber;
    }
}