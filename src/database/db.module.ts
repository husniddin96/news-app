import { Module } from '@nestjs/common';
import { KnexService } from './db.service';

@Module({
    providers: [KnexService],
    exports: [KnexService]
})
export class DBModule { }
