import { Injectable } from '@nestjs/common';
import { Knex, knex } from 'knex';

@Injectable()
export class KnexService {
    private instance: any;

    constructor() {
        this.instance = knex({
            client: 'postgresql',
            debug: false,
            connection: {
                host: process.env.DB_HOST,
                user: process.env.DB_USER,
                database: process.env.DB_NAME,
                password: process.env.DB_PASSWORD,
                port: Number(process.env.DB_PORT),
            },
            pool: {
                min: 1,
                max: Number(process.env.DB_MAX_POOL_SIZE) || 4,
            },
        });
    }

    public getInstance(): Knex {
        return this.instance;
    }
}
