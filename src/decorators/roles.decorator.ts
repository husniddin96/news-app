// roles.ts
import { SetMetadata } from '@nestjs/common';
import { UserRole } from 'src/users/user.interface';

export const Roles = (...roles: UserRole[]) => SetMetadata('roles', roles);