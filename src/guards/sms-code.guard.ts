import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { SMSService } from 'src/auth/sms.service';

@Injectable()
export class SMSCodeGuard implements CanActivate {
    constructor(
        private reflector: Reflector,
        private readonly smsService: SMSService
    ) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();

        const code = request.headers['code'];
        const phone = request.headers['phone'];

        if (!code || !phone) {
            return Promise.resolve(false);
        }

        return await this.smsService.checkOTP(phone, code);
    }

}