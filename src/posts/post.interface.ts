export enum PostStatus {
    DRAFT = 'DRAFT',
    PUBLISHED = 'PUBLISHED'
}

export interface CreatePostDto {
    title: string;
    cover: string;
    description: string;
    full_text: string;
    author_id: string;
    status?: PostStatus;
}

export interface UpdatePostDto {
    title?: string;
    cover?: string;
    description?: string;
    full_text?: string;
    status?: PostStatus;
}

export interface PostStatusDTO {
    status: PostStatus;
}