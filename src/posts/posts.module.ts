import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { DBModule } from 'src/database/db.module';
import { KnexService } from 'src/database/db.service';
import { PostsRepo } from './posts.repo';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [DBModule, JwtModule],
  controllers: [PostsController],
  providers: [PostsService, PostsRepo, KnexService],
})
export class PostsModule { }
