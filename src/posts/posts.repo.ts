import { Injectable } from '@nestjs/common';
import { KnexService } from 'src/database/db.service';
import { UpdatePostDto } from './post.interface';

@Injectable()
export class PostsRepo {
  private readonly tableName = 'posts'
  constructor(
    private knexService: KnexService,
  ) { }

  async create(post) {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName).insert(post);
    return result;
  }

  async findAll() {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName).select('*');
    return result;
  }

  async findById(id: number) {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName)
      .select('*')
      .where('id', id)
      .first();

    return result;
  }

  async updateById(id: number, payload: UpdatePostDto) {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName)
      .update(payload)
      .where('id', id);

    return result;
  }
}
