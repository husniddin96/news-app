import { Injectable } from '@nestjs/common';
import { CreatePostDto, PostStatus, PostStatusDTO, UpdatePostDto } from './post.interface';
import { PostsRepo } from './posts.repo';

@Injectable()
export class PostsService {
  constructor(private readonly repo: PostsRepo) { }

  create(post: CreatePostDto) {
    post.status = PostStatus.DRAFT;

    return this.repo.create(post);
  }

  findAll() {
    return this.repo.findAll();
  }

  findOne(id: number) {
    return this.repo.findById(id);
  }

  updateById(id: number, post: UpdatePostDto) {
    return this.repo.updateById(id, post);
  }

  publish(id: number, postStatus: PostStatusDTO) {
    return this.repo.updateById(id, postStatus);
  }

}
