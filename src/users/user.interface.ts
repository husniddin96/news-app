


export enum UserRole {
    ADMIN = 'ADMIN',
    MODERATOR = 'MODERATOR',
    USER = 'USER',
}

export enum UserStatus {
    ACTIVE = 'ACTIVE',
    BANNED = 'BANNED'
}

export interface User {
    id: number;
    role: UserRole;
    phone: string;
    email: string;
    full_name: string;
    password: string;
    status: UserStatus
}

export interface CreateUserDTO {
    role: string;
    phone: string;
    email: string;
    password: string;
}

// export interface ChangeUserRoleDTO {
//     id?: number;
//     phone?: string;
//     role: UserRole;
// }

// export interface ChangeUserStatusDTO {
//     id?: number;
//     phone?: string;
//     status: UserStatus;
// }

export interface UpdateUserDTO {
    id?: number;
    phone?: string;
    status?: UserStatus;
    role?: UserRole;
}
