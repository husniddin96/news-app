import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { DBModule } from 'src/database/db.module';
import { KnexService } from 'src/database/db.service';
import { UsersRepo } from './users.repo';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [DBModule, JwtModule],
  controllers: [UsersController],
  providers: [UsersService, UsersRepo, KnexService],
  exports: [UsersService, UsersRepo]
})
export class UsersModule { }
