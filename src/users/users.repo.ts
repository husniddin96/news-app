import { Injectable } from '@nestjs/common';
import { KnexService } from 'src/database/db.service';
import { CreateUserDTO, UpdateUserDTO, User } from './user.interface';

@Injectable()
export class UsersRepo {
  private readonly tableName = 'users';
  constructor(
    private knexService: KnexService,
  ) { }

  async create(user: CreateUserDTO) {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName).insert(user);

    return result;
  }

  async findAll() {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName).select('*');

    return result;
  }

  async findById(id: number) {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName)
      .select('*')
      .where('id', id);

    return result;
  }

  async findByPhone(phone: string): Promise<User | null> {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName)
      .select('*')
      .where('phone', phone)
      .first();

    return result;
  }

  async findByEmail(email: string) {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName)
      .select('*')
      .where('email', email)
      .first();

    return result;
  }

  async updateById(id: number, payload: UpdateUserDTO) {
    const knex = this.knexService.getInstance();
    const result = await knex(this.tableName)
      .update(payload)
      .where('id', payload.id);

    return result;
  }
}
