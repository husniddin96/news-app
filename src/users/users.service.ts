import { Injectable } from '@nestjs/common';
import { CreateUserDTO, UpdateUserDTO } from './user.interface';
import { UsersRepo } from './users.repo';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(private readonly repo: UsersRepo) { }

  async create(user: CreateUserDTO) {
    const saltOrRounds = 10;
    const hash = await bcrypt.hash(user.password, saltOrRounds);

    user.password = hash;
    return this.repo.create(user);
  }

  findAll() {
    return this.repo.findAll();
  }

  findById(id: number) {
    return this.repo.findById(id);
  }

  findByPhone(phone: string) {
    return this.repo.findByPhone(phone);
  }

  findByEmail(email: string) {
    return this.repo.findByEmail(email);
  }

  update(id: number, updateUserDto: UpdateUserDTO) {
    return this.repo.updateById(id, updateUserDto);
  }

}
